# DOTS


The Drone Operation Template Specification language (DOTS) is designed to describe multi-UAS missions.
DOTS started as a master project by Niels Christian Heltner and Niclas Schilling Mølby at SDU, Maersk Mc-Kinney Moller institute, fall 2019 - spring 2020. Supervisor: Ulrik Pagh Schultz.

DOTS is developed using [Xtext](https://www.eclipse.org/Xtext/).
The program is translated to an XML representation, which is then used by a planner inside of [ROS-DOTS](https://gitlab.com/m.campusano/dots_system), a service oriented architecture to support BVLOS multi-UAS missions.

## Installation

**TODO**

## Usage

**TODO**

## Considerations and Limitations

* Nested loops have not been tested: DOTS allows its use
* Multidimensional arrays are not supported:  DOTS does not allow its use
