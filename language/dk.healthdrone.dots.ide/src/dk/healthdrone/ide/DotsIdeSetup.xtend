/*
 * generated by Xtext 2.25.0
 */
package dk.healthdrone.ide

import com.google.inject.Guice
import dk.healthdrone.DotsRuntimeModule
import dk.healthdrone.DotsStandaloneSetup
import org.eclipse.xtext.util.Modules2

/**
 * Initialization support for running Xtext languages as language servers.
 */
class DotsIdeSetup extends DotsStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new DotsRuntimeModule, new DotsIdeModule))
	}
	
}
