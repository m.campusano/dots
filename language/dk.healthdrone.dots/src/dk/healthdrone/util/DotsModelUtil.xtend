package dk.healthdrone.util

import dk.healthdrone.dots.UsingDroneDefinition
import dk.healthdrone.dots.ForLoopIterableDefinition
import dk.healthdrone.dots.Parameter
import org.eclipse.emf.ecore.EClass
import dk.healthdrone.dots.NamedDeclarationReference
import dk.healthdrone.dots.DataType
import dk.healthdrone.dots.DotsPackage

class DotsModelUtil {
	val epackage = DotsPackage.eINSTANCE
	
	def dispatch boolean isNonArrayType(Parameter parameter, EClass type){
		if(!parameter.isVariableArray)
			return parameter.isTypeOrArrayType(type)
		return false
	}
	
	def dispatch boolean isNonArrayType(ForLoopIterableDefinition definition, EClass type ) {
		definition.getParameter.isNonArrayType(type)
	}
	
	def dispatch boolean isNonArrayType(UsingDroneDefinition definition, EClass type ) {
		definition.getParameter.isNonArrayType(type)
	}
	
	def dispatch boolean isTypeOrArrayType(Parameter parameter, EClass type ) {
		type == parameter.dataType.eClass || parameter.dataType.eClass.getEAllSuperTypes.contains(type)
	}
	
	def dispatch boolean isTypeOrArrayType(ForLoopIterableDefinition definition, EClass type ) {
		definition.getParameter.isTypeOrArrayType(type)
	}
	
	def dispatch boolean isTypeOrArrayType(UsingDroneDefinition definition, EClass type ) {
		definition.getParameter.isTypeOrArrayType(type)
	}
	
	def dispatch boolean isArrayType(Parameter parameter, EClass type ) {
		parameter.dataType.isArray && parameter.isTypeOrArrayType(type)
	}
	
	def dispatch boolean isArrayType(ForLoopIterableDefinition definition, EClass type ) {
		val parameter = definition.getParameter
		parameter.isArrayType(type)
	}
	
	def dispatch boolean isArrayType(UsingDroneDefinition definition, EClass type ) {
		val parameter = definition.getParameter
		parameter.isArrayType(type)
	}
	
	def dispatch Parameter getParameter(Parameter parameter) {
		parameter
	}
	
	def dispatch Parameter getParameter(ForLoopIterableDefinition definition) {
		definition.iterable.reference.getParameter
	}
	
	def dispatch Parameter getParameter(UsingDroneDefinition definition) {
		definition.iterable.reference.getParameter
	}
	
	
	def boolean match(NamedDeclarationReference[] references, DataType[] types){

		if(references.length != types.length)
			return false;
		
		for(var i = 0 ; i<references.length ; i++){
			if(references.get(i).reference.eClass == epackage.namedDeclaration)
				return false;
			val referencedParameter = references.get(i).reference.getParameter
			if(references.get(i).reference.isVariableArray != types.get(i).isArray 
				|| !referencedParameter.isTypeOrArrayType(types.get(i).eClass))
				return false;
		}
		return true
	}
	
	def dispatch boolean isVariableArray(Parameter parameter) {
		return parameter.dataType.isArray
	}
	
	def dispatch boolean isVariableArray(ForLoopIterableDefinition definition) {
		return false
	}
	
	def dispatch boolean isVariableArray(UsingDroneDefinition definition) {
		return false
	}
}