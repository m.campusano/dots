/*
 * generated by Xtext 2.25.0
 */
package dk.healthdrone.validation

import com.google.inject.Inject
import dk.healthdrone.dots.ActionCall
import dk.healthdrone.dots.Assignment
import dk.healthdrone.dots.Coordination
import dk.healthdrone.dots.DotsPackage
import dk.healthdrone.dots.ForLoop
import dk.healthdrone.dots.GeoConstraint
import dk.healthdrone.dots.GeoInsideConstraint
import dk.healthdrone.dots.GeoOutsideConstraint
import dk.healthdrone.dots.Operation
import dk.healthdrone.dots.Parameter
import dk.healthdrone.dots.TimeAfterConstraint
import dk.healthdrone.dots.TimeBeforeConstraint
import dk.healthdrone.dots.TimeConstraint
import dk.healthdrone.util.DotsModelUtil
import org.eclipse.xtext.validation.Check

/** 
 * This class contains custom validation rules. 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class DotsValidator extends AbstractDotsValidator {
	@Inject extension DotsModelUtil
	
	public static final String TYPE_MISMATCH = "typeMismatch"
	public static final String INVALID_CALL = 'invalidCall'
	public static final String PARAMETER_NAME = 'parameterName'
	
	val epackage = DotsPackage.eINSTANCE

	@Check 
	def void assignmentOnlyOnDrone(Assignment assignment) {
		if(!assignment.left.reference.isTypeOrArrayType(epackage.drone)){
			error("Actions should be called on a drone or list of drones",
		            DotsPackage.Literals.ASSIGNMENT__LEFT,
		            TYPE_MISMATCH);
		}
		
	}
	
	@Check
	def void foorLoopIterableOfLocation(ForLoop loop){
		if(!loop.location.iterable.reference.isArrayType(epackage.location)){
			error("ForLoops only iterate from array of location",
		            DotsPackage.Literals.FOR_LOOP__LOCATION,
		            TYPE_MISMATCH);
		}
	}
	
	@Check
	def void foorLoopDrone(ForLoop loop){
		if(!loop.drone.iterable.reference.isArrayType(epackage.drone)){
			error("ForLoops only use array of drones",
		            DotsPackage.Literals.FOR_LOOP__DRONE,
		            TYPE_MISMATCH);
		}
	}
	
	@Check
	def void actionCallArguments(ActionCall call){
		if(!call.arguments.match(call.action.dataTypes)){
			error("Mismatch of arguments in action call",
		            DotsPackage.Literals.ACTION_CALL__ARGUMENTS,
		            INVALID_CALL);
		}
	}
	
	@Check
	def void uniqueOperationParameters(Parameter parameter){
		val operation = parameter.eContainer as Operation
		for (Parameter other : operation.parameters){
			if(other != parameter && other.name == parameter.name){
				error("Parameters name have to be unique",
		            DotsPackage.Literals.NAMED_DECLARATION__NAME,
		            PARAMETER_NAME)
		            return;
		     }
	    }
	}
	
	@Check
	def void coordinationDroneArgument(Coordination coordination){
		if(!coordination.drone.iterable.reference.isArrayType(epackage.drone)){
			error("Coordination can only use array of drones",
		            DotsPackage.Literals.COORDINATION__DRONE,
		            TYPE_MISMATCH);
		}
	}
	
	@Check
	def void insideConstraintAreaArgument(GeoInsideConstraint constraint){
		geoConstraintAreaArgument(constraint, "Inside constraint can only use area or list of areas")
	}
	
	@Check
	def void insideConstraintAreaArgument(GeoOutsideConstraint constraint){
		geoConstraintAreaArgument(constraint, "Outside constraint can only use area or list of areas")
	}
	
	@Check
	def void beforeConstraintTimeArgument(TimeBeforeConstraint constraint){
		timeConstraintTimeArgument(constraint, "Before constraint can only use time")
	}
	
	@Check
	def void afterConstraintTimeArgument(TimeAfterConstraint constraint){
		timeConstraintTimeArgument(constraint, "After constraint can only use time")
	}
	
	def protected void geoConstraintAreaArgument(GeoConstraint constraint, String errorMessage){
		if(!constraint.area.reference.isTypeOrArrayType(epackage.area)){
			error(errorMessage,
		            DotsPackage.Literals.GEO_CONSTRAINT__AREA,
		            TYPE_MISMATCH);
		}
	}
	
	def protected void timeConstraintTimeArgument(TimeConstraint constraint, String errorMessage){
		if(!constraint.time.reference.isNonArrayType(epackage.time)){
			error(errorMessage,
		            DotsPackage.Literals.TIME_CONSTRAINT__TIME,
		            TYPE_MISMATCH);
		}
	}
}
