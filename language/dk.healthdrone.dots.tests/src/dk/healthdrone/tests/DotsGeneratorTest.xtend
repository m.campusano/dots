package dk.healthdrone.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.xbase.testing.CompilationTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(DotsInjectorProvider)

class DotsGeneratorTest {
	@Inject extension CompilationTestHelper
	
	
	@Test
	def void compileOperation() {
		'''
			operation opTest:
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
			</operation>
			'''
		)
	}
	
	@Test
	def void compileDroneParameter() {
		'''
			operation opTest:
				uav: drone
				uavs: drone[]
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<parameter type="Drone" array="false" name="uav" />
				<parameter type="Drone" array="true" name="uavs" />
			</operation>
			'''
		)
	}
	
	@Test
	def void compilePointParameter() {
		'''
			operation opTest:
				point: navigation_point
				points: navigation_point[]
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<parameter type="NavigationPoint" array="false" name="point" />
				<parameter type="NavigationPoint" array="true" name="points" />
			</operation>
			'''
		)
	}
	
	@Test
	def void compileAreaParameter() {
		'''
			operation opTest:
				zone: area
				zones: area[]
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<parameter type="Area" array="false" name="zone" />
				<parameter type="Area" array="true" name="zones" />
			</operation>
			'''
		)
	}
	
	@Test
	def void compileImplementation() {
		'''
			operation opTest:
				implementation:
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<mission>
				</mission>
			</operation>
			'''
		)
	}
	
	@Test
	def void compileActionDefinition() {
		'''operation opTest:
				import action noArguments
				import action arguments<location[]>
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<action-definition name="noArguments">
				</action-definition>
				<action-definition name="arguments">
					<parameter type="Location" array="true" />
				</action-definition>
			</operation>
			'''
		)
	}
	
	@Test
	def void compileActionExecution() {
		'''
			operation opTest:
				import action noArguments
				import action arguments<location[]>
				
				uav: drone
				pointA: navigation_point
				pointB: navigation_point
				implementation:
					uav += noArguments()
					uav += arguments(pointA, pointB)
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<action-definition name="noArguments">
				</action-definition>
				<action-definition name="arguments">
					<parameter type="Location" array="true" />
				</action-definition>
				<parameter type="Drone" array="false" name="uav" />
				<parameter type="NavigationPoint" array="false" name="pointA" />
				<parameter type="NavigationPoint" array="false" name="pointB" />
				<mission>
					<sequential>
						<action name="noArguments">
							<left reference="uav" />
						</action>
						<action name="arguments">
							<left reference="uav" />
							<argument reference="pointA" />
							<argument reference="pointB" />
						</action>
					</sequential>
				</mission>
			</operation>
			'''
		)
	}
	
	@Test
	def void compileForLoopTypes() {
		'''
			operation opTest:
				fields: area[]
				implementation:
					parallel for field in fields
					end
					
					prioritized for field in fields
					end
					
					sequential for field in fields
					end
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<parameter type="Area" array="true" name="fields" />
				<mission>
					<sequential>
						<for type="parallel" location="field" location-list="fields">
						</for>
						<for type="prioritized" location="field" location-list="fields">
						</for>
						<for type="sequential" location="field" location-list="fields">
						</for>
					</sequential>
				</mission>
			</operation>
			'''
		)
	}
	
	@Test
	def void compileForLoop() {
		'''
			operation opTest:
				import action straight<location[]>

				uav: drone
				fields: area[]
				implementation:
					parallel for field in fields
						uav += straight(field)
					end
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<action-definition name="straight">
					<parameter type="Location" array="true" />
				</action-definition>
				<parameter type="Drone" array="false" name="uav" />
				<parameter type="Area" array="true" name="fields" />
				<mission>
					<sequential>
						<for type="parallel" location="field" location-list="fields">
							<action name="straight">
								<left reference="uav" />
								<argument reference="field" />
							</action>
						</for>
					</sequential>
				</mission>
			</operation>
			'''
		)
	}
	
	@Test
	def compileForLoopUsingDrones() {
		'''
			operation opTest:
				uavs: drone[]
				fields: area[]
				implementation:
					parallel for field in fields using selectedUav from uavs
					end
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<parameter type="Drone" array="true" name="uavs" />
				<parameter type="Area" array="true" name="fields" />
				<mission>
					<sequential>
						<for type="parallel" location="field" location-list="fields" drone="selectedUav" drone-list="uavs">
						</for>
					</sequential>
				</mission>
			</operation>
			'''
			
		)
	}
	
		@Test
	def void compileCoordinationTypes() {
		'''
			operation opTest:
				implementation:
					parallel
					end
					
					prioritized
					end
					
					sequential
					end
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<mission>
					<sequential>
						<parallel>
						</parallel>
						<prioritized>
						</prioritized>
						<sequential>
						</sequential>
					</sequential>
				</mission>
			</operation>
			'''
		)
	}
	@Test
	def compileCoordination() {
		'''
			operation opTest:
				import action straight<location[]>
				uav1: drone
				uav2: drone
				field: area
				implementation:
					parallel
						uav1 += straight(field)
						uav2 += straight(field)
					end
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<action-definition name="straight">
					<parameter type="Location" array="true" />
				</action-definition>
				<parameter type="Drone" array="false" name="uav1" />
				<parameter type="Drone" array="false" name="uav2" />
				<parameter type="Area" array="false" name="field" />
				<mission>
					<sequential>
						<parallel>
							<action name="straight">
								<left reference="uav1" />
								<argument reference="field" />
							</action>
							<action name="straight">
								<left reference="uav2" />
								<argument reference="field" />
							</action>
						</parallel>
					</sequential>
				</mission>
			</operation>
			'''
		)
	}

	@Test
	def compileCoordinationUsingDrones() {
		'''
			operation opTest:
				uavs: drone[]
				implementation:
					parallel using uav from uavs
					end
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<parameter type="Drone" array="true" name="uavs" />
				<mission>
					<sequential>
						<parallel drone="uav" drone-list="uavs">
						</parallel>
					</sequential>
				</mission>
			</operation>
			'''
			)
	}

	@Test
	def	compilationGeoInsideConstraint() {
		'''
			operation opTest:
				geofence: area
				implementation:
					constraint inside geofence
					end
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<parameter type="Area" array="false" name="geofence" />
				<mission>
					<sequential>
						<constraint type="Geofence" variable="geofence">
						</constraint>
					</sequential>
				</mission>
			</operation>
			'''
		)
	}

	@Test
	def compilationGeoOutsideConstraint() {
		'''
			operation opTest:
				noFlyZone: area
				implementation:
					constraint outside noFlyZone
					end
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<parameter type="Area" array="false" name="noFlyZone" />
				<mission>
					<sequential>
						<constraint type="NoFlyZone" variable="noFlyZone">
						</constraint>
					</sequential>
				</mission>
			</operation>
			'''
		)
	}

	@Test
	def	compilationTimeBeforeConstraint() {
		'''
			operation opTest:
				before_time: time
				implementation:
					constraint before before_time
					end
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<parameter type="Time" array="false" name="before_time" />
				<mission>
					<sequential>
						<constraint type="EndTime" variable="before_time">
						</constraint>
					</sequential>
				</mission>
			</operation>
			'''
		)
	}

	@Test
	def	compilationTimeAFterConstraint() {
		'''
			operation opTest:
				after_time: time
				implementation:
					constraint after after_time
					end
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<parameter type="Time" array="false" name="after_time" />
				<mission>
					<sequential>
						<constraint type="StartTime" variable="after_time">
						</constraint>
					</sequential>
				</mission>
			</operation>
			'''
		)
	}

	@Test
	def compilationConstraintBody() {
		'''
			operation opTest:
				import action straight<location[]>
				geofence: area
				uav: drone
				points: navigation_point[]
				implementation:
					constraint inside geofence
						uav += straight(points)
					end
		'''.assertCompilesTo(
			'''
			<operation name="opTest">
				<action-definition name="straight">
					<parameter type="Location" array="true" />
				</action-definition>
				<parameter type="Area" array="false" name="geofence" />
				<parameter type="Drone" array="false" name="uav" />
				<parameter type="NavigationPoint" array="true" name="points" />
				<mission>
					<sequential>
						<constraint type="Geofence" variable="geofence">
							<action name="straight">
								<left reference="uav" />
								<argument reference="points" />
							</action>
						</constraint>
					</sequential>
				</mission>
			</operation>
			'''
		)
	}
}