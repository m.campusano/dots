package dk.healthdrone.tests

import com.google.inject.Inject
import dk.healthdrone.dots.DotsPackage
import dk.healthdrone.dots.Operation
import org.eclipse.xtext.diagnostics.Diagnostic
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(DotsInjectorProvider)
class DotsScopeTest {
	@Inject extension ParseHelper<Operation>
	@Inject extension ValidationTestHelper
	
	val epackage = DotsPackage.eINSTANCE
	
	@Test
	def void variableBeforeForLoop() {
		'''
			operation opTest:
				import action straight<location>
				uas: drone
				points: navigation_point[]
				implementation:
					uas += straight(point)
					parallel for point in points
					
					end
		'''.parse.assertError(epackage.namedDeclarationReference, 
            Diagnostic.LINKING_DIAGNOSTIC
        )
		

	}
	
	@Test
	def void variableAfterForLoop() {
		'''
			operation opTest:
				import action straight<location>
				uas: drone
				points: navigation_point[]
				implementation:
					parallel for point in points
					
					end
					uas += straight(point)
		'''.parse

	}
}